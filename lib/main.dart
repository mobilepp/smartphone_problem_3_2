import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';

import 'course_list.dart';
import 'media_query_info.dart';
import 'aspect_ratio_example.dart';
import 'aspect_ratio_layout_builder.dart';
// import 'flutter_is_love_lb.dart';
import 'flutter_is_love_mdq.dart';
import 'video_player.dart';

void main() {
  runApp(const CoursesApp());
}

class CoursesApp extends StatelessWidget {
  const CoursesApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'A smartphone app',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: const CourseList(),
        // home: Scaffold(
        //   backgroundColor: Colors.white,
        //   appBar: AppBar(
        //     title: const Text('MediaQuery'),
        //   ),
        //   body: const SafeArea(
        //     child: MediaQueryInfo(),
        //   ),
        // ),
        // home: Scaffold(
        //   backgroundColor: Colors.white,
        //   appBar: AppBar(
        //     title: const Text('AspectRatio'),
        //   ),
        //   body: const SafeArea(
        //     child: AspectRatioExample(),
        //   ),
        // ),
        // home: Scaffold(
        //   backgroundColor: Colors.white,
        //   appBar: AppBar(
        //     title: const Text('LayoutBuilder'),
        //   ),
        //   body: const SafeArea(
        //     child: AspectRatioLayoutBuilder(),
        //   ),
        // ),
        // home: Scaffold(
        //   backgroundColor: Colors.white,
        //   appBar: AppBar(
        //     title: const Text('Challenge'),
        //   ),
        //   body: const SafeArea(
        //     child: Center(child: FlutterIsLove(size: 200.0)),
        //   ),
        // ),

        // home: Scaffold(
        //   backgroundColor: Colors.white,
        //   appBar: AppBar(
        //     title: const Text('Challenge'),
        //   ),
        //   body: const VideoPlayer(),
        // ),
      ),
    );
  }
}
