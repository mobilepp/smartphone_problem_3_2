import 'package:flutter/material.dart';
import 'app_layout.dart';
import 'dart:math';

class SettingsView extends StatelessWidget {
  const SettingsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
      ),
      body: LayoutBuilder(builder: (context, constraints) {
        final totalWidth = constraints.maxWidth;
        final extraContentPadding =
            max<double>(totalWidth - AppLayout.maximumContentWidthDetail, 0) /
                2;
        return
            // ListView(
            //   padding: EdgeInsets.symmetric(horizontal: extraContentPadding),
            //   children: <Widget>[
            //     ListTile(
            //       leading: const Icon(Icons.notifications),
            //       title: const Text('Push notifications'),
            //       trailing: Switch(value: true, onChanged: (_) {}),
            //     ),
            //     ListTile(
            //       leading: const Icon(Icons.notifications),
            //       title: const Text('NightLight'),
            //       trailing: Switch(value: true, onChanged: (_) {}),
            //     ),
            //     ListTile(
            //       leading: const Icon(Icons.notifications),
            //       title: const Text('Alarm!'),
            //       trailing: Switch(value: true, onChanged: (_) {}),
            //     ),
            //   ],
            // );
            Container(
                constraints: const BoxConstraints(
                  maxWidth: 500,
                ),
                child: ListView(
                  children: <Widget>[
                    ListTile(
                      leading: const Icon(Icons.notifications),
                      title: const Text('Push notifications'),
                      trailing: Switch(value: true, onChanged: (_) {}),
                    ),
                    ListTile(
                      leading: const Icon(Icons.notifications),
                      title: const Text('NightLight'),
                      trailing: Switch(value: true, onChanged: (_) {}),
                    ),
                    ListTile(
                      leading: const Icon(Icons.notifications),
                      title: const Text('Alarm!'),
                      trailing: Switch(value: true, onChanged: (_) {}),
                    ),
                  ],
                ));
      }),
      // body: ListView(
      //   padding: EdgeInsets.zero,
      //   children: <Widget>[
      //     ListTile(
      //       leading: const Icon(Icons.notifications),
      //       title: const Text('Push notifications'),
      //       trailing: Switch(value: true, onChanged: (_) {}),
      //     ),
      //   ],
      // ),
    );
  }
}
