import 'package:flutter/material.dart';

var smallerHeightDevice = false;

class VideoPlayer extends StatefulWidget {
  const VideoPlayer({Key? key}) : super(key: key);

  @override
  State<VideoPlayer> createState() => _VideoPlayerState();
}

class _VideoPlayerState extends State<VideoPlayer> {
  int num = 0;
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    smallerHeightDevice = screenHeight < screenWidth;

    final video = AspectRatio(
      aspectRatio: 16 / 9,
      child: Container(
        color: Colors.black,
        child: const Center(
          child: Icon(
            Icons.play_circle,
            color: Colors.white,
          ),
        ),
      ),
    );

    return smallerHeightDevice
        ? Row(
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                    color: Colors.white,
                  )),
              Expanded(flex: 5, child: video),
              Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      IconButton(
                        onPressed: () {
                          setState(() {
                            ++num;
                          });
                        },
                        icon: const Icon(
                          Icons.info,
                          color: Colors.green,
                        ),
                      ),
                      // Text("$num"),
                      Container(
                        child: num % 2 == 0 ? null : VideoDescription(),
                      )
                    ],
                  ))
            ],
          )
        : Column(
            children: [
              video,
              const VideoDescription(),
            ],
          );
  }
}

class VideoDescription extends StatelessWidget {
  const VideoDescription({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(
            'Video Title',
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: smallerHeightDevice ? 24 * 0.6 : 24),
          ),
          Text(
            'Video description',
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: smallerHeightDevice ? 18 * 0.6 : 18),
          ),
          Text(
            'Likes: 34',
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: smallerHeightDevice ? 16 * 0.6 : 16),
          ),
        ],
      ),
    );
  }
}

//import 'package:flutter/material.dart';

// class VideoPlayer extends StatelessWidget {
//   const VideoPlayer({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       children: [
//         AspectRatio(
//           aspectRatio: 16 / 9,
//           child: Container(
//             color: Colors.black,
//             child: const Center(
//               child: Icon(
//                 Icons.play_circle,
//                 color: Colors.white,
//               ),
//             ),
//           ),
//         ),
//         const VideoDescription(),
//       ],
//     );
//   }
// }

// class VideoDescription extends StatelessWidget {
//   const VideoDescription({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.all(8.0),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.stretch,
//         children: const [
//           Text(
//             'Video Title',
//             overflow: TextOverflow.ellipsis,
//             style: TextStyle(fontSize: 24),
//           ),
//           Text(
//             'Video description',
//             overflow: TextOverflow.ellipsis,
//             style: TextStyle(fontSize: 18),
//           ),
//           Text(
//             'Likes: 34',
//             overflow: TextOverflow.ellipsis,
//             style: TextStyle(fontSize: 16),
//           ),
//         ],
//       ),
//     );
//   }
// }
